#!/usr/bin/env bash

all: dev run

fmt:
	goimports -l -w  ./app

usevendor:
	#vendor = $(shell if [  -d "vendor" ]; then echo "exist"; else echo "notexist"; fi;)
	if [ ! -d "vendor" ]; then  mv /tmp/vendor.refund_service vendor ; fi;
	govendor update code.qschou.com/qschou/...
	govendor remove code.qschou.com/qschou/refund_service/...
	govendor add +e
	govendor remove +u


novendor:
	#vendor = $(shell if [  -d "vendor" ]; then echo "exist"; else echo "notexist"; fi;)
	if [  -d "vendor" ]; then  mv vendor /tmp/vendor.refund_service ; fi;

install: fmt clean

clean:
	rm -rf output/conf/
gotest:
	 go test ./... | grep -v "^ok "

dev: install
	go build -o output/bin/refund_service ./app

linux:
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o go_rpc_demo ./app

test: install
	go build -tags qsc_test -o output/bin/refund_service ./app

release: install
	go build -tags qsc_live -o output/bin/refund_service ./app
run:
	ETCD_ADDR=121.41.52.161:12379 output/bin/refund_service
