module liuhe.io/go_practice

go 1.15

require (
	github.com/alibabacloud-go/bssopenapi-20171214/v3 v3.0.0
	github.com/alibabacloud-go/darabonba-openapi/v2 v2.0.2
	github.com/alibabacloud-go/tea v1.1.20
	github.com/alibabacloud-go/tea-utils/v2 v2.0.4
	github.com/chanxuehong/util v0.0.0-20200304121633-ca8141845b13
	github.com/golang/protobuf v1.3.2
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/panjf2000/ants/v2 v2.4.6
	github.com/shenshouer/ftp4go v0.0.0-20230323014503-421554c4a041
	github.com/shopspring/decimal v1.3.1
	github.com/wechatpay-apiv3/wechatpay-go v0.2.14
	go.etcd.io/bbolt v1.3.5 // indirect
	go.etcd.io/etcd v0.0.0-20191205234010-1f8764be3b43
	go.uber.org/zap v1.14.1
	golang.org/x/net v0.7.0
	golang.org/x/text v0.7.0
	google.golang.org/grpc v1.24.0
)
