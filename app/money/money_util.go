package main

import (
	"fmt"
	"github.com/chanxuehong/util/money"
	"regexp"
	"strconv"
)

func main() {
	text := "44.7301"
	fen1 := yuan2Fen(text)
	fmt.Println(fen1)

	text2 := "0.04"
	fen2 := yuan2Fen(text2)
	fmt.Println(fen2)
}

func yuan2Fen(text string) money.Money2 {
	tmpData := text
	decimal, _ := checkDecimal(text, 4)
	if decimal {
		fmt.Println("isFourDot...", text)
		float, _ := strconv.ParseFloat(text, 64)
		tmpData = fmt.Sprintf("%.2f", float)
	}
	var moneyStr money.Money2
	_ = moneyStr.UnmarshalTextString(tmpData)
	return moneyStr
}

// CheckDecimal 判断小数点后几位小数点
// str 所要判断的 小数字符串
// dotBehind 所要判断的 位数
func checkDecimal(str string, dotBehind int) (bool, error) {
	return regexp.MatchString(fmt.Sprintf("^((0|[1-9][0-9]*))(\\.[0-9]{%d})$", dotBehind), str)
}
