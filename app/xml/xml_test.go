package xml

import (
	"encoding/xml"
	"fmt"
	"testing"
)

type QueryWXRedPacketReceiveRecordReply struct {
	Hblists struct {
		Hbinfo []*Hblist `xml:"hbinfo"`
	} `xml:"hblist"`

	// 企业微信红包新增字段
	Openid  string `xml:"openid" json:"openid,omitempty"`
	Amount  int64  `xml:"amount" json:"amount,omitempty"`
	RcvTime string `xml:"rcv_time" json:"rcv_time,omitempty"`
}

type Hblist struct {
	Openid  string `xml:"openid" json:"openid,omitempty"`
	Amount  int64  `xml:"amount" json:"amount,omitempty"`
	RcvTime string `xml:"rcv_time" json:"rcv_time,omitempty"`
}

func TestXmlTest(t *testing.T) {
	certReply := new(QueryWXRedPacketReceiveRecordReply)
	result := "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg><result_code><![CDATA[SUCCESS]]></result_code><err_code><![CDATA[SUCCESS]]></err_code><err_code_des><![CDATA[OK]]></err_code_des><mch_billno><![CDATA[9010080799701411170000046603]]></mch_billno><mch_id><![CDATA[11475856]]></mch_id><detail_id><![CDATA[10000417012016080830956240040]]></detail_id><status><![CDATA[RECEIVED]]></status><send_type><![CDATA[ACTIVITY]]></send_type><hb_type><![CDATA[NORMAL]]></hb_type><total_num>1</total_num><total_amount>100</total_amount><openid><![CDATA[oHkLxtzmyHXX6FW_cAWo_orTSRXs]]></openid><amount>100</amount><rcv_time><![CDATA[2016-08-08 21:49:46]]></rcv_time><send_time><![CDATA[2016-08-08 21:49:22]]></send_time><hblist><hbinfo><openid><![CDATA[oHkLxtzmyHXX6FW_cAWo_orTSRXs]]></openid><amount>100</amount><rcv_time><![CDATA[2016-08-08 21:49:46]]></rcv_time></hbinfo></hblist></xml>"
	err := xml.Unmarshal([]byte(result), &certReply)
	if err != nil {
		fmt.Println(err)
		return
	}
	println(certReply.Openid)
}
