package main

import (
	"context"
	"fmt"
	"github.com/wechatpay-apiv3/wechatpay-go/core"
	"github.com/wechatpay-apiv3/wechatpay-go/core/option"
	"github.com/wechatpay-apiv3/wechatpay-go/services/transferbatch"
	"time"

	"github.com/wechatpay-apiv3/wechatpay-go/utils"
	"log"
)

/*
登陆商户平台【API安全】->【API证书】->【查看证书】，可查看商户API证书序列号。
商户API证书和微信支付平台证书均可以使用第三方的证书解析工具，查看证书内容。或者使用openssl命令行工具查看证书序列号。
$ openssl x509 -in 1900009191_20180326_cert.pem -noout -serial
serial=1DDE55AD98ED71D6EDD4A4A16996DE7B47773A8C
*/

func main() {
	var (
		mchID                      string = "1525950841"                               // 商户号
		mchCertificateSerialNumber string = "" // 商户证书序列号
		mchAPIv3Key                string = ""         // 商户APIv3密钥
	)

	// 使用 utils 提供的函数从本地文件中加载商户私钥，商户私钥会用来生成请求的签名，这个证书需要PRIVATE KEY格式
	mchPrivateKey, err := utils.LoadPrivateKeyWithPath("/Users/liuhe/工作文档/03-证书服务/微信证书/1525950841/1525950841_pri_key.pem")
	if err != nil {
		log.Fatal("load merchant private key error")
	}

	ctx := context.Background()
	// 使用商户私钥等初始化 client，并使它具有自动定时获取微信支付平台证书的能力
	opts := []core.ClientOption{
		option.WithWechatPayAutoAuthCipher(mchID, mchCertificateSerialNumber, mchPrivateKey, mchAPIv3Key),
	}
	client, err := core.NewClient(ctx, opts...)
	if err != nil {
		log.Fatalf("new wechat pay client err:%s", err)
	}

	// h5支付
	//svc := h5.H5ApiService{Client: client}
	//resp, result, err := svc.QueryOrderById(ctx,
	//	h5.QueryOrderByIdRequest{
	//		TransactionId: core.String("4200000795202101117281490871"),
	//		Mchid:         core.String("1525950841"),
	//	},
	//)
	//
	//if err != nil {
	//	// 处理错误
	//	log.Printf("call QueryOrderById err:%s", err)
	//} else {
	//	// 处理返回结果
	//	log.Printf("status=%d resp=%s", result.Response.StatusCode, resp)
	//}

	// 微信支付平台证下载
	//svc := certificates.CertificatesApiService{Client: client}
	//resp, result, err := svc.DownloadCertificates(ctx)
	//log.Printf("status=%d resp=%s", result.Response.StatusCode, resp)
	//certificate := resp.Data[0].EncryptCertificate
	//gcm, err := utils.DecryptAES256GCM(mchAPIv3Key, *certificate.AssociatedData, *certificate.Nonce, *certificate.Ciphertext)
	//if err != nil {
	//	log.Println(err)
	//}
	//log.Println(gcm)

	// 根据批次号查询
	svc := transferbatch.TransferDetailApiService{Client: client}
	resp, result, err := svc.GetTransferDetailByOutNo(ctx,
		transferbatch.GetTransferDetailByOutNoRequest{
			OutDetailNo: core.String("x23zy545Bd5436"),
			OutBatchNo:  core.String("plfk2020042013"),
		},
	)

	if err != nil {
		//apiError := core.APIError{}
		//err2 := json.Unmarshal([]byte(err.Error()), &apiError)
		//if nil != err2 {
		//	println("aaaaaaaaaaaaaa", err2)
		//}
		//log.Printf(apiError.Code)
		// 处理错误
		log.Printf("call GetTransferDetailByOutNo err:%s", err.Error())
	} else {
		// 处理返回结果
		log.Printf("status=%d resp=%s", result.Response.StatusCode, resp)
	}
	var tmpVar = "hello"
	t := &TransferDetailEntity{Mchid: &tmpVar}
	fmt.Println(*t.Mchid)

}

type TransferDetailEntity struct {
	// 微信支付分配的商户号
	Mchid *string `json:"mchid"`
	// 商户系统内部的商家批次单号，在商户系统内部唯一
	OutBatchNo *string `json:"out_batch_no"`
	// 微信批次单号，微信商家转账系统返回的唯一标识
	BatchId *string `json:"batch_id"`
	// 申请商户号的appid或商户号绑定的appid（企业号corpid即为此appid）
	Appid *string `json:"appid"`
	// 商户系统内部区分转账批次单下不同转账明细单的唯一标识
	OutDetailNo *string `json:"out_detail_no"`
	// 微信支付系统内部区分转账批次单下不同转账明细单的唯一标识
	DetailId *string `json:"detail_id"`
	// PROCESSING:转账中。正在处理中，转账结果尚未明确   SUCCESS:转账成功   FAIL:转账失败。需要确认失败原因后，再决定是否重新发起对该笔明细单的转账（并非整个转账批次单）
	DetailStatus *string `json:"detail_status"`
	// 转账金额单位为“分”
	TransferAmount *int64 `json:"transfer_amount"`
	// 单条转账备注（微信用户会收到该备注），UTF8编码，最多允许32个字符
	TransferRemark *string `json:"transfer_remark"`
	// 商户appid下，某用户的openid
	Openid *string `json:"openid"`
	// 收款方姓名。采用标准RSA算法，公钥由微信侧提供 商户转账时传入了收款用户姓名、查询时会返回收款用户姓名
	UserName *string `json:"user_name,omitempty" encryption:"EM_APIV3"`
	// 转账发起的时间，按照使用rfc3339所定义的格式，格式为YYYY-MM-DDThh:mm:ss+TIMEZONE
	InitiateTime *time.Time `json:"initiate_time"`
	// 明细最后一次状态变更的时间，按照使用rfc3339所定义的格式，格式为YYYY-MM-DDThh:mm:ss+TIMEZONE
	UpdateTime *time.Time `json:"update_time"`
}
