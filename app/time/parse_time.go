package main

import (
	"encoding/json"
	"fmt"
	"net/url"
	"time"
)

const (
	TimeFormatDateTime = "2006-01-02 15:04:05" //日期时间格式化基准时间
)

func main() {
	urlValues := url.Values{}
	urlValues.Add("key1", "广西农村信用社(合作银行)")
	urlValues.Del("Sign")
	urlValues.Del("SignType")
	// 兼容java服务请求过来没有参与签名问题
	urlValues.Del("ServiceFee")
	urlValues.Del("ServiceRate")
	urlValues.Del("ProjectState")
	fmt.Println(urlValues.Encode())

	tMinusOne := time.Now().Add(-time.Hour * time.Duration(24*1))
	startOfDay := time.Date(tMinusOne.Year(), tMinusOne.Month(), tMinusOne.Day(), 0, 0, 0, 0, tMinusOne.Location())
	// 获取前一天的结束时间（即当天23:59:59）
	endOfDay := startOfDay.Add(24*time.Hour - 1*time.Second)

	fmt.Println(startOfDay.Format(TimeFormatDateTime))
	fmt.Println(endOfDay.Format(TimeFormatDateTime))

	//parseTime()
	//diffTime("1389055222202")

	// golang对“*”进行urlEnocde为”%2A“
	//values := url.Values{}
	//values.Add("key1", "广西农村信用社(合作银行)")
	//fmt.Println(values.Encode())
	//fmt.Println(url.QueryEscape("key1=&$-_.+!*'(),&key2=*"))
}

var USE_NEW_PLATFORM_MCHIDS = []string{"1395773702", "1517151581", "1389055202"}

func diffTime(mchId string) {
	cacheCert := "{\"mch_id\":\"1389055202\",\"infos\":{\"3F18FE8F7A2DB3046A9DF6E7DECD8EC7E7EA5EED\":{\"serial_no\":\"3F18FE8F7A2DB3046A9DF6E7DECD8EC7E7EA5EED\",\"effective_time\":\"2023-10-18T16:54:37+08:00\",\"expire_time\":\"2028-10-16T16:54:37+08:00\",\"encrypt_certificate\":{\"algorithm\":\"AEAD_AES_256_GCM\",\"nonce\":\"f93c36392a2d\",\"associated_data\":\"certificate\",\"ciphertext\":\"6MX4ELKMBe+nCa46hcmTLuaD19+Cp0W/h/jDPWVz0Jp2Mfgu9z/AkOCmcs8ecEuWTtstTlpR5d1IbNP4cVj1jKKM+MYtlKFYkMOMXMQQZcChWxN/Nf51ucVhsWheU8NaNfnqLsOwtAZnNws/ml0aw2f66a1uvuzE7odGeA8r/ie5GnhWGvReOrKjphx3epqA7M2QlIAQKWOEvURRt5T1QKSDf13DM52AgTBGje1d2ek8AtgtaXv0wBEaw5+PXflH7FAVLKAj8GHW8H4Uw47D2Bg/yKNVCSY/NNKlhLYexx01ryLUeQAMhWKIOaHpSnfF2RZJdKBNNDdOnkXA5W9ejtE1d2/J0tQx16hHvEjgWOe/BLqJpuqd3twm1WLxdWq1KNjgHsJ4vkpQX4OC/NbbjdsTIdd+nakelFBZsvbdnv35nYDcexAhMaQ1BTVphRyCaE8nOoicaCwDibpTsyK6V5P3QYuKltpTzrTHJfcDsc8XFpvaJYST8arNZ9/SXVQQ8LlTXjLMtCL0p5wUAF4TeT8OLrhxWz9+8U8wb/kIS4352WkFs1bW16TOG7TBmvajwtGLWj6sGN280QahavaeVxOuRwBXBXzyC6C9x9mtXraFmYyHFd59g+VSscddMbSsYVx1wRrzMvzAUTxro8ikIH8P5au5NyiD+cSU3YTAxX4P6HrZ+lGjIAxZmKZLpzhmbS5HZ2FVuS+kZhbNe2Nn5SbDD8OxuTq+rsfq2BLwe7Eo3B3+9XbumWOanU2fNtO00xR4qFJFEelr/PS1w4uoUoS05d1sLoY/KlIjEYTlTaI+dN10ScETfz0a8Hcedu+XSUi16wc7DJFHRWFGW481hL44NtoGN1SAx26F/4hC1v11OwJ7nVvz+1SG0N0hXT+2ELssn35UCQwCM4bUPqm97WIYs8lXkgtfSqrKNxdOl4+el4BTLQeNEcqlxNCBdTSRxC6mjKPoCGA020zBysX1S86YHTPkJXqSeFYWq66Ln7PafEJ0QG0YRwW1aMIL54cjAq2APCXe+ef4Yl/7kHzRgEX1trkSuixvQ6k/IIh4UvyniEAUn9ppKVaZaqtwkZkKGjQ/uulmveXkNvko7f4RXwMwd5SfZ2IR13dFUIRNClJTzDtszDPvPIihsYAl/zPhcreLqAauVHmYnWa2eJd1l7QGY65ggtppv+/uAL7uY0j//ofyTdSv9+miJiip1Vo4CBixo6lJalsZUsR6V1wUYSfHhwsDfphvVheJiMIxLxT87Y3BsalQjeKGjTJPH/GmWh5n+qoP0pQ/NLPWB9ati18+P0ZsT+3ew/uOCSUkBAA0tJpOmYeZ0gYo5RC5DD5M4unONNGOUO8yP4ZcJCha5+TZ+iynL1CvwYHq+IlmWwlg9Dn0NsCJDnl8mGuCwthYLvZOosUyMmQai8TLGrzrtatezZ8Xu/FH9gtjKjgTCq2Mp3I+XpG0AybhThlNJ/5lPGffVmBxp1B0lKF9SwLKpLCQZawDMQCVpd0aHV5xod5R5exPPMIT36HXZvMCsLp7B6bktKG4kDAmoFaAcJUsPpsrkW6K2XluJDStOZWkcNsKrNs05Yry0NjUNPtCJ9VWykbP5Li4n93UsMmO5EaUpASbEegSqXXI3Gpzd9ijCkOhZ+pnk3du1BlZllbSWLVtMM8lDyHk6iSSfn41cK2EXWYmGT6Ecwr/d7p0ztNSe4Ibxtc8CVgSevhY41yorkj+JX/bsNIXVhnnnFk5j7zh7fpsOpRQj1AOnd8NJIKqPXvO0eQB6bNsWw0wmPiIdJjV0myFmox7tyYDuqqFipj+ybHeLdXAjToR0OpAuFrTuEvrvpfJpMssQMNwYmY8kE87c/FviF3+5hQWLuFWFcTukix4JIXl2ZIrLlPxa1KOTuZKOAHaB5L0X41GM45OxN1SKLKErKC1pB9fceuwsUmeESSd1z/Zb7QgjFl4YWdVNVk+py4128kpXKckxuZakTckzowL\"}},\"45FD55CB3B0DF92E4C4399915A9B94768FFEAC33\":{\"serial_no\":\"45FD55CB3B0DF92E4C4399915A9B94768FFEAC33\",\"effective_time\":\"2018-11-13T14:32:05+08:00\",\"expire_time\":\"2023-11-12T14:32:05+08:00\",\"encrypt_certificate\":{\"algorithm\":\"AEAD_AES_256_GCM\",\"nonce\":\"ad3d631e7b5c\",\"associated_data\":\"certificate\",\"ciphertext\":\"3mJEhwszdgo7LvrMnJsgaE3KKuoGClco+Oy9yzwD1tjY1wCqjgbAUHRbrJ32eZ9k7B6+TKN7TRlKUhNKVqNif4OnkIFLSY9Fdwoyjscjf3HQyvSEHcdtu+zpFYIpTI/fH7j9qUiMU+jEXWvJOP2OOEt494bMa6klQnpoi2PBISK8l6W4ZBFBlt0G+PqV8Eyhwmo8btCvio6kktN8SKwk6nBTxA8vBsgX6Uig2NQ3X48rfl0p7SpDPREx8wGwgKFzjyXRi/jxhrO8vCKa1HktnY5wYf5HvaFmXOsjrsgKAuHgZ8PA6U6DTS5lWAovEth99JislSY4PrdhgEGHGV92lOpma178jJWFYn6r5HEPUpegfsDPv1uhOeJ3nHWpzkw2MeiO3zz/HdV6a8NstQNMIT0q+eSb19/zzI62SUaGcFPoXLveOPVppNPnbV0Gt9+79gwzYUmHyE4wf1S0Y2Dr6IM8o9Orz9LGCgiai02++AFz+fGdFQnDBd2L4UEZk631227wI/JVsq/yMTNVtcoo40PV05LsYeTSPU66uxxBbLOYcoR9tCVvXoKcNI+eBmyU0v9iexneGVm1peAc5QmNSzbmcwiDMNKZvjBMhn4BIG+kkXoLhOLTsvlVObQro/U3rloQyFeOfD4dFePq9cldtNZ685zTFs88qrGHq4gmeF4TgPLaQyIuvH+5ftEGnEiWmctrU7WRsGjTg/7EO7nXplSOVlIazdCcvaDtmlELhXARui6/SW9FZIvZ1uaUvmcQL5hdgPFqcgKpN357jyIIifJYI4ATl+YrtNKZcqoLICXJzD21tw824b2hzXhCH9bPYF0lbfN8JUVKl8Yj0Amc/OPTuqw6VTHODMn9DAVPdEATrsKqvCL7BTxnBNMJ7TdlyMZw7jv2/L4PRgtL/AuhMoph/blWZO3COUwVPuZFakFOYzr8GoBtoXIZTFSrieyRs8b8CMegmj102IMnAv70MWC6boH4po7ykCMF+zaSaWH9emefAkCeO2kt/HtTZdnwo44S6x2WU+7QwwCM+fQ0939ucaw2w27MmGhquD9KfIKVWXu4/bSB/77z/OrY7aGHRuVR47G5z0FBzoQPFGHr4U+9gn3nxSLIlQDqDw/fSE2wC/bXjvJ9lBxJ+8wkjYDV1GmK70ozBsuxEIJUlIHHxhB9oD+DuNjmjMJdcfyx3tAvGBocCA7N+mswJuRj8hDOGFRIN2N2oy6akqhhKfMCJQ3ZFrilMFq0WL0prp5yy4ftRMEShHZUU0yHrd3YiM1R/qZjarao6o69Rbyd4ndsbgPI0CGZOXPGW6mz/LTmnFDqy+z3vtDIAGU2l31w0nvMxob9kA6+lmYzsZU0vrGkenrVZoeDiqWMX4JbX2OlLA30fsBdLAlawcxb7KWD1UF3gv57oHqxkDH5p+Pm0p4woWq93JOarNE4e9Oq73XKtuw38UflEguO15a0d/5HYxS0iLRpmdgL/j2OFNO2yF7oFOR1wpyEz6koP3Fc/wkcWQDe8PpxH9kQqi/In7sScRPJMlfwjLQ4DG5oelCNAL5qsoHxhpdQIfH+YGGOlXMERP+Sp+vU7tZLeR2X+Q+Xz6sVTcTJYTRYxP+OLNmYa3mWLp8vAV/rZODsewf9ApO0bu0/0KxhgjRXkXdrJj6k3/ycB2XT4cTu/EVR5gol58rZSdH5nvge7n5BeqmM8tNu+jH7+AKf8+HYBShzcYRvL9R24osWmeMKBTqgMmYNdfxvLyamFiB9bZVX1pInGia70cEjxNNViwt+wYc63Nm8fDH9Y4Nz0oGtAbGF+BXYeZxj/WYWJ8GO2tvmDzKl/eI70A9H5E2e4ZmBSo/HMT2Pi8H9OswhNm8L4tXEQHfdkkHelChpIAAdupDaGJAdaMv7SWMMrM5B2jA6gdhq2zAYvMqmiob7\"}}}}"
	//cacheCert := "{\"mch_id\":\"1389055202\",\"infos\":{\"45FD55CB3B0DF92E4C4399915A9B94768FFEAC33\":{\"serial_no\":\"45FD55CB3B0DF92E4C4399915A9B94768FFEAC33\",\"effective_time\":\"2018-11-13T14:32:05+08:00\",\"expire_time\":\"2023-11-12T14:32:05+08:00\",\"encrypt_certificate\":{\"algorithm\":\"AEAD_AES_256_GCM\",\"nonce\":\"ad3d631e7b5c\",\"associated_data\":\"certificate\",\"ciphertext\":\"3mJEhwszdgo7LvrMnJsgaE3KKuoGClco+Oy9yzwD1tjY1wCqjgbAUHRbrJ32eZ9k7B6+TKN7TRlKUhNKVqNif4OnkIFLSY9Fdwoyjscjf3HQyvSEHcdtu+zpFYIpTI/fH7j9qUiMU+jEXWvJOP2OOEt494bMa6klQnpoi2PBISK8l6W4ZBFBlt0G+PqV8Eyhwmo8btCvio6kktN8SKwk6nBTxA8vBsgX6Uig2NQ3X48rfl0p7SpDPREx8wGwgKFzjyXRi/jxhrO8vCKa1HktnY5wYf5HvaFmXOsjrsgKAuHgZ8PA6U6DTS5lWAovEth99JislSY4PrdhgEGHGV92lOpma178jJWFYn6r5HEPUpegfsDPv1uhOeJ3nHWpzkw2MeiO3zz/HdV6a8NstQNMIT0q+eSb19/zzI62SUaGcFPoXLveOPVppNPnbV0Gt9+79gwzYUmHyE4wf1S0Y2Dr6IM8o9Orz9LGCgiai02++AFz+fGdFQnDBd2L4UEZk631227wI/JVsq/yMTNVtcoo40PV05LsYeTSPU66uxxBbLOYcoR9tCVvXoKcNI+eBmyU0v9iexneGVm1peAc5QmNSzbmcwiDMNKZvjBMhn4BIG+kkXoLhOLTsvlVObQro/U3rloQyFeOfD4dFePq9cldtNZ685zTFs88qrGHq4gmeF4TgPLaQyIuvH+5ftEGnEiWmctrU7WRsGjTg/7EO7nXplSOVlIazdCcvaDtmlELhXARui6/SW9FZIvZ1uaUvmcQL5hdgPFqcgKpN357jyIIifJYI4ATl+YrtNKZcqoLICXJzD21tw824b2hzXhCH9bPYF0lbfN8JUVKl8Yj0Amc/OPTuqw6VTHODMn9DAVPdEATrsKqvCL7BTxnBNMJ7TdlyMZw7jv2/L4PRgtL/AuhMoph/blWZO3COUwVPuZFakFOYzr8GoBtoXIZTFSrieyRs8b8CMegmj102IMnAv70MWC6boH4po7ykCMF+zaSaWH9emefAkCeO2kt/HtTZdnwo44S6x2WU+7QwwCM+fQ0939ucaw2w27MmGhquD9KfIKVWXu4/bSB/77z/OrY7aGHRuVR47G5z0FBzoQPFGHr4U+9gn3nxSLIlQDqDw/fSE2wC/bXjvJ9lBxJ+8wkjYDV1GmK70ozBsuxEIJUlIHHxhB9oD+DuNjmjMJdcfyx3tAvGBocCA7N+mswJuRj8hDOGFRIN2N2oy6akqhhKfMCJQ3ZFrilMFq0WL0prp5yy4ftRMEShHZUU0yHrd3YiM1R/qZjarao6o69Rbyd4ndsbgPI0CGZOXPGW6mz/LTmnFDqy+z3vtDIAGU2l31w0nvMxob9kA6+lmYzsZU0vrGkenrVZoeDiqWMX4JbX2OlLA30fsBdLAlawcxb7KWD1UF3gv57oHqxkDH5p+Pm0p4woWq93JOarNE4e9Oq73XKtuw38UflEguO15a0d/5HYxS0iLRpmdgL/j2OFNO2yF7oFOR1wpyEz6koP3Fc/wkcWQDe8PpxH9kQqi/In7sScRPJMlfwjLQ4DG5oelCNAL5qsoHxhpdQIfH+YGGOlXMERP+Sp+vU7tZLeR2X+Q+Xz6sVTcTJYTRYxP+OLNmYa3mWLp8vAV/rZODsewf9ApO0bu0/0KxhgjRXkXdrJj6k3/ycB2XT4cTu/EVR5gol58rZSdH5nvge7n5BeqmM8tNu+jH7+AKf8+HYBShzcYRvL9R24osWmeMKBTqgMmYNdfxvLyamFiB9bZVX1pInGia70cEjxNNViwt+wYc63Nm8fDH9Y4Nz0oGtAbGF+BXYeZxj/WYWJ8GO2tvmDzKl/eI70A9H5E2e4ZmBSo/HMT2Pi8H9OswhNm8L4tXEQHfdkkHelChpIAAdupDaGJAdaMv7SWMMrM5B2jA6gdhq2zAYvMqmiob7\"}},\"3F18FE8F7A2DB3046A9DF6E7DECD8EC7E7EA5EED\":{\"serial_no\":\"3F18FE8F7A2DB3046A9DF6E7DECD8EC7E7EA5EED\",\"effective_time\":\"2023-10-18T16:54:37+08:00\",\"expire_time\":\"2028-10-16T16:54:37+08:00\",\"encrypt_certificate\":{\"algorithm\":\"AEAD_AES_256_GCM\",\"nonce\":\"f93c36392a2d\",\"associated_data\":\"certificate\",\"ciphertext\":\"6MX4ELKMBe+nCa46hcmTLuaD19+Cp0W/h/jDPWVz0Jp2Mfgu9z/AkOCmcs8ecEuWTtstTlpR5d1IbNP4cVj1jKKM+MYtlKFYkMOMXMQQZcChWxN/Nf51ucVhsWheU8NaNfnqLsOwtAZnNws/ml0aw2f66a1uvuzE7odGeA8r/ie5GnhWGvReOrKjphx3epqA7M2QlIAQKWOEvURRt5T1QKSDf13DM52AgTBGje1d2ek8AtgtaXv0wBEaw5+PXflH7FAVLKAj8GHW8H4Uw47D2Bg/yKNVCSY/NNKlhLYexx01ryLUeQAMhWKIOaHpSnfF2RZJdKBNNDdOnkXA5W9ejtE1d2/J0tQx16hHvEjgWOe/BLqJpuqd3twm1WLxdWq1KNjgHsJ4vkpQX4OC/NbbjdsTIdd+nakelFBZsvbdnv35nYDcexAhMaQ1BTVphRyCaE8nOoicaCwDibpTsyK6V5P3QYuKltpTzrTHJfcDsc8XFpvaJYST8arNZ9/SXVQQ8LlTXjLMtCL0p5wUAF4TeT8OLrhxWz9+8U8wb/kIS4352WkFs1bW16TOG7TBmvajwtGLWj6sGN280QahavaeVxOuRwBXBXzyC6C9x9mtXraFmYyHFd59g+VSscddMbSsYVx1wRrzMvzAUTxro8ikIH8P5au5NyiD+cSU3YTAxX4P6HrZ+lGjIAxZmKZLpzhmbS5HZ2FVuS+kZhbNe2Nn5SbDD8OxuTq+rsfq2BLwe7Eo3B3+9XbumWOanU2fNtO00xR4qFJFEelr/PS1w4uoUoS05d1sLoY/KlIjEYTlTaI+dN10ScETfz0a8Hcedu+XSUi16wc7DJFHRWFGW481hL44NtoGN1SAx26F/4hC1v11OwJ7nVvz+1SG0N0hXT+2ELssn35UCQwCM4bUPqm97WIYs8lXkgtfSqrKNxdOl4+el4BTLQeNEcqlxNCBdTSRxC6mjKPoCGA020zBysX1S86YHTPkJXqSeFYWq66Ln7PafEJ0QG0YRwW1aMIL54cjAq2APCXe+ef4Yl/7kHzRgEX1trkSuixvQ6k/IIh4UvyniEAUn9ppKVaZaqtwkZkKGjQ/uulmveXkNvko7f4RXwMwd5SfZ2IR13dFUIRNClJTzDtszDPvPIihsYAl/zPhcreLqAauVHmYnWa2eJd1l7QGY65ggtppv+/uAL7uY0j//ofyTdSv9+miJiip1Vo4CBixo6lJalsZUsR6V1wUYSfHhwsDfphvVheJiMIxLxT87Y3BsalQjeKGjTJPH/GmWh5n+qoP0pQ/NLPWB9ati18+P0ZsT+3ew/uOCSUkBAA0tJpOmYeZ0gYo5RC5DD5M4unONNGOUO8yP4ZcJCha5+TZ+iynL1CvwYHq+IlmWwlg9Dn0NsCJDnl8mGuCwthYLvZOosUyMmQai8TLGrzrtatezZ8Xu/FH9gtjKjgTCq2Mp3I+XpG0AybhThlNJ/5lPGffVmBxp1B0lKF9SwLKpLCQZawDMQCVpd0aHV5xod5R5exPPMIT36HXZvMCsLp7B6bktKG4kDAmoFaAcJUsPpsrkW6K2XluJDStOZWkcNsKrNs05Yry0NjUNPtCJ9VWykbP5Li4n93UsMmO5EaUpASbEegSqXXI3Gpzd9ijCkOhZ+pnk3du1BlZllbSWLVtMM8lDyHk6iSSfn41cK2EXWYmGT6Ecwr/d7p0ztNSe4Ibxtc8CVgSevhY41yorkj+JX/bsNIXVhnnnFk5j7zh7fpsOpRQj1AOnd8NJIKqPXvO0eQB6bNsWw0wmPiIdJjV0myFmox7tyYDuqqFipj+ybHeLdXAjToR0OpAuFrTuEvrvpfJpMssQMNwYmY8kE87c/FviF3+5hQWLuFWFcTukix4JIXl2ZIrLlPxa1KOTuZKOAHaB5L0X41GM45OxN1SKLKErKC1pB9fceuwsUmeESSd1z/Zb7QgjFl4YWdVNVk+py4128kpXKckxuZakTckzowL\"}}}}"
	//cacheCert := "{\"mch_id\":\"1322851101\",\"infos\":{\"390C795CE9C17D190B43D94F3348AE6BE6604FC1\":{\"serial_no\":\"390C795CE9C17D190B43D94F3348AE6BE6604FC1\",\"effective_time\":\"2019-02-15T13:58:14+08:00\",\"expire_time\":\"2024-02-14T13:58:14+08:00\",\"encrypt_certificate\":{\"algorithm\":\"AEAD_AES_256_GCM\",\"nonce\":\"362d13ada4d6\",\"associated_data\":\"certificate\",\"ciphertext\":\"9Bh6uEgbkzv/fIGe0Ut1cqBVTjJrJXdMYtqtXm15AwLcHDrgbnZfmAGC7IDh5jNgMP54N7q0Mq5S2JIEUAndz/QyPOzhMjpH80IDWEH3C8dwrS2vTGEQDEeJsmFLc8ZHkLJpeOd2a9iQfWviyDTxFwozskdqv99hwdi/GfSDEr2WtjedleXGMq+byGz8kndBICOMRSuYsJt9y3tdC8NcdWg8ZlDKHDVgURV035UDZO81Ovsk4lJHDnIfTGIuijo9YfTsq9aYmfr7MaYyFeX0hP9/x/fIoUF5k0wJoj3mXVo7KcyNgAebQDbCSpKsJHiLClbKl/txS+9TG1rABBXDzkh5ckqZzPOEr5qb5zwFVI+n76FOUxJV44nh/BgJKHtNsltXk6NVv+ywFXu9OYIKc4YoeTkf4YgRBMO6tstf3bkSREHBqDlLyA29xxfRhKJ+JVzXUo1JkquQra6mlSTW2/AWBMhbiBan9WDfbUs8LuISgI3rfKQt8dQfT3HJALPWXG7viRz9yl7InrF0HD8EX5nuMx6AhrM4HuVRa8Xd5estapY9oY7CVq4zKQcPEDtvXEIdsaykMzDO9FRtBVKjd2Wm3KI+fWtLEXdaNNWA3UWnoHc8H/ONY7t2kIFZMRCWLbOHcEugCHMVdetQOz7wgQbUG5rnKpn4QfkOhNxA0Z/dQlWTULbYxwk2bEmQyZqd7HuTWdv1CK+omUkOAbywjlJto+VAnCBLO6A3COTeCU5dGXl4dfRBKVY1Ggc9Afe2cT6vPNzbzXAUX9+rVowH3FLjqRYLWMaWI/sl0mUV452NwFVxlyGw6n7IqAvf/MMDBfMU69azl2KdA3xmqBxngeY/9DlQvALYed9Sd9sHk+cy/CYME9BNVKukhjcFYN6Mhm1PwGRQLiGkfuDSdIIvo2PO9Bmv2X1Y/ooMqHkYH3y+vcKSBHLbHVplUxyCmNmbVLAK1Hvg0hS3IkCh4SdnyBkD30wB1QaQbc/JwEhtIt66ndXI53N4PuLp6S5IIxk5mpmQQMuHvXYvs8SA3tbNvOZJw81Fa+BodxuXOk5pG/qDYp5L12PbmEkDR663lNqNceoHyktCqf5eww+NtF/FxQ3OQDPAaspTc6bruWipsOeIoJ8tr/PFpAMvJ1US6eDmEnrx2KXJX/7oZdJ/RJ+630xDGia06PCEZcOc+wzLrHQ/dqDeERJ0R4ZcbIV2Exj7a0iw7anU3YwY1eUbyo5Hu8zZtMGx3f8hSeAFer3nqVtES2f6zG8XlX1FO6s6fQRc8kIP2qvssfunJyS2+hOQ79WhquBjb9R8G/EeuUiViQ250Cx9+zX2J+ygg1Tq7BSCNh84KT1ECUQj+s82Np+Lmoi9i47CJ85Sp0+1KFoLj4tTmoUWa5rjRRmQbdaTv5f/0kQVDQA6g+Ecvs//xTGE9i8nZdiQz4QuYE4HaWZR4rfqZfyAYmn6Wi/Q0GsgNFKkX0WCEEId2IURc86MIZVKRQRRmx9sZwYNXCm8zAnCf5jSLpozzSrbfAReac8qtKSgUle8GPB8PU/JYuSNVpMAYWedRCIHyu1vxA5JBOXoLIR//LZVRFZaOQeg1Gk4eQIPDX1JfvK71clLO+rzjyWJyrqkzGxfMZP7HvlNZf/ZOOERZJnMZCTHfWp3DMMYghu4mjwmNbcJwduwXNtAfF7Q7vqaugUR5oU0WKH9wzz4D8num/DCzFsCaazqw9O6qMky+kKYhKalpk8091SQ/H1sCFzYVMz0ht4go21HgftGWhuNqgTJn0UL0xk+TAq0ibM6SrAIq7I7uZ2O/c6iQu+X/GAc5w0Ls+DW1ZxpOEklJLeHXv4Mx1VSCONbafwwc3+IWVnTsLB9pdGcu+jNOBnQHCokVneO6Wmmq8RBQ/92VZV4rsan2Dxvg6ZrNyjamyhf6inx\"}}}}"
	v := CertInfo{}
	if err := json.Unmarshal([]byte(cacheCert), &v); err != nil {
		return
	}

	useNewPlatform := false
	for _, value := range USE_NEW_PLATFORM_MCHIDS {
		if value == mchId {
			useNewPlatform = true
		}
	}
	certInfo := Data{}
	if useNewPlatform {
		// 使用最新平台证书
		for _, v := range v.Infos {
			// 默认取第1个证书
			certInfo = v
			break
		}
		if len(v.Infos) > 1 {
			for _, crt := range v.Infos {
				// 获取最新且有效的证书
				if (crt.EffectiveTime.After(certInfo.EffectiveTime)) && (time.Now().Before(crt.ExpireTime) || time.Now().Equal(crt.ExpireTime)) {
					certInfo = crt
				}
			}
		}
	} else {
		// 原有逻辑
		for _, crt := range v.Infos {
			if certInfo.EffectiveTime.Before(crt.EffectiveTime) {
				certInfo = crt
			}
		}
	}
	fmt.Println("mchId", v.MchId, "serialNo", certInfo.SerialNo, "useNewPlatform", useNewPlatform)
}

func parseTime() {
	now := time.Now()
	now2 := time.Now()

	println("----after----", now.After(now2))
	println("----before----", now.Before(now2))

	date := now.Add(-time.Hour * time.Duration(24*7)).Format("20060102")
	fmt.Println(date)

	var timeStr = "2022-11-14T15:47:49+08:00"

	timeObj1, _ := time.Parse(TimeFormatDateTime, timeStr)
	fmt.Println(timeObj1)

	timeObj2, _ := time.ParseInLocation(TimeFormatDateTime, timeStr, time.Local)
	fmt.Println(timeObj2)

	t1, _ := time.Parse(
		time.RFC3339,
		timeStr)
	fmt.Println(t1.String())

	// 当前时间time转2023-01-13T14:47:45+08:00
	t := time.Now()
	fmt.Println(t.Format(time.RFC3339))

	//先将时间转换为字符串
	// 2023-01-13T14:47:45+08:00 转成 2023-01-13 14:47:45
	tt, _ := time.Parse(time.RFC3339, timeStr)
	fmt.Println(tt.Format(TimeFormatDateTime))
}
