package main

import (
	"crypto/x509"
	"time"
)

type CertInfo struct {
	MchId string          `json:"mch_id"`
	Infos map[string]Data `json:"infos"`
}

type Data struct {
	SerialNo           string             `json:"serial_no"`
	EffectiveTime      time.Time          `json:"effective_time"`
	ExpireTime         time.Time          `json:"expire_time"`
	EncryptCertificate EncryptCertificate `json:"encrypt_certificate"`
	Cert               *x509.Certificate  `json:"-"`
}

type EncryptCertificate struct {
	Algorithm      string `json:"algorithm"`
	Nonce          string `json:"nonce"`
	AssociatedData string `json:"associated_data"`
	Ciphertext     string `json:"ciphertext"`
}
