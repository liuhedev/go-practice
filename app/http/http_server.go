package main

import (
	"fmt"
	"net/http"
	"runtime"
	"time"

	//导入echo包
	"github.com/labstack/echo"
)

func main() {
	//实例化echo对象。
	e := echo.New()

	//注册一个Get请求, 路由地址为: /liuhedev  并且绑定一个控制器函数, 这里使用的是闭包函数。
	e.GET("/liuhedev", func(c echo.Context) error {
		//控制器函数直接返回一个字符串，http响应状态为http.StatusOK，就是200状态。
		return c.String(http.StatusOK, "欢迎访问liuhedev.com")
	})

	// 异步处理请求
	e.GET("/async", func(echo echo.Context) error {
		// copy请求
		go func() {
			time.Sleep(1 * time.Second)
			fmt.Println("异步执行：", echo.Request().RequestURI)
		}()
		goroutine := runtime.NumGoroutine()
		fmt.Println("协程数量", goroutine)
		return echo.JSON(200, fmt.Sprintf("执行成功%d", goroutine))
	})

	//启动http server, 并监听1204端口，冒号（:）前面为空的意思就是绑定网卡所有Ip地址，本机支持的所有ip地址都可以访问。
	e.Start(":1204")
}
