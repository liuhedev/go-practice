package main

import "time"

func main() {
	println("start")
	demo(1)
	println("end")
}

func demo(i int) {
	println("demo")
	i++
	time.Sleep(500 * time.Millisecond)
	//if i ==100 {
	//	return
	//}
	demo(i)
}
