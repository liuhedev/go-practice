package main

import (
	"fmt"
)

func worker(c chan int) {
	//从channel c中得到数据
	num := <-c
	//time.Sleep(5 * time.Second)
	fmt.Println("得到了管道中的数据 ", num)
}

// 协程不会等待信号处理完毕
func testRoutine() {
	//创建⼀个channel
	c := make(chan int)

	//开辟一个协程，去执行worker函数
	go worker(c)
	//main向channel c中写数据
	c <- 1

	fmt.Println("我是main")
}
