package main

import (
	"fmt"
	"go.uber.org/zap"
	"time"
)

func main() {
	fmt.Println("main process start~~")
	logger, _ := zap.NewProduction()
	defer logger.Sync()
	logger.Info("无法获取网址",
		zap.String("url", "http://www.baidu.com"),
		zap.Int("attempt", 3),
		zap.Duration("backoff", time.Second),
	)
	//testBasicRoutine()
	//testRoutine()
	handleCoreLogic()

	testPoolWork()

	// 子协程如果有耗时操作，那如果main函数执行完毕，子协程可能执行不完毕；所以增加此耗时
	time.Sleep(3000 * time.Millisecond)

	fmt.Println("main process end~~")

}

func handleCoreLogic() {
	fmt.Println("handle start")
	time.Sleep(500 * time.Millisecond)
	go func() {
		time.Sleep(1000 * time.Millisecond)
		fmt.Println("go routine~~~")
	}()

	fmt.Println("handle end")
}
