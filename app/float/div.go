package main

import (
	"fmt"
	"strconv"
)

func main() {
	waitTime := float32(3) / 25
	fmt.Println(waitTime)

	// https://blog.csdn.net/qq_38769551/article/details/101459811
	//ASCII字符集由95个可打印字符（0x20-0x7E）和33个控制字符（0x00-0x1F，0x7F）组成
	s := string(2)
	fmt.Println(s)

	formatInt := strconv.FormatInt(2, 10)
	fmt.Println(formatInt)

	fmt.Println(fmt.Sprint(2))
}
