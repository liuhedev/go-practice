package main

import (
	"encoding/json"
	"fmt"
	"time"
)

func init() {
	fmt.Println("--------first--------")
}

type Class struct {
	Name  string
	Grade int
}

//钉钉配置
var DingConfig map[string]AlarmRobot

type AlarmRobot struct {
	AccessToken string `json:"accessToken"`
	Secret      string `json:"secret"`
}

func main() {
	//test1()
	//test2()

	//var strV = "1234"
	//fmt.Println("strV的地址：", &strV, "strV的值：", strV)
	//changV(&strV)
	//fmt.Println("strV的地址：", &strV, "strV的值：", strV)
	//fmt.Println(strV)

	//testParse()
	testParse3()
}

func testParse3() {
	str := "{\"default\":{\"accessToken\":\"23\",\"secret\":\"SECdf363b0dc8acb6c93e0563f8fad88ad9bf1642ea036462bd0b8123e725770751\"},\"notEnough\":{\"accessToken\":\"f4c5ebe5fbbf4ec8cec40e32e1ef751d3100bb3cc15a95d3f356a4667d58bdb1\",\"secret\":\"SECdf363b0dc8acb6c93e0563f8fad88ad9bf1642ea036462bd0b8123e725770751\"},\"amtLimit\":{\"accessToken\":\"f4c5ebe5fbbf4ec8cec40e32e1ef751d3100bb3cc15a95d3f356a4667d58bdb1\",\"secret\":\"SECdf363b0dc8acb6c93e0563f8fad88ad9bf1642ea036462bd0b8123e725770751\"}}"
	if err := json.Unmarshal([]byte(str), &DingConfig); err != nil {
		fmt.Println(err)
	}
	println(DingConfig["default"].Secret)
}

func testParse() {
	c := new(Class)
	data := "{\"Name\":\"liuhe\",\"Grade\":70}"
	err := json.Unmarshal([]byte(data), c)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(c.Name)
}

func testParse2() (class *Class) {
	data := "{\"Name\":\"liuhe\",\"Grade\":70}"
	err := json.Unmarshal([]byte(data), &class)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(class.Name)
	return
}

func changV(v *string) {
	fmt.Println("v的地址：", v)
	vv := *v
	fmt.Println("vv的地址：", &vv)
	fmt.Println(vv)

	*v = "hello"

	fmt.Println(vv)

}

func test1() {
	entity := TransferDetailEntity{}
	fmt.Println(entity)
	fmt.Println(&entity)
	//
	////nwe (T)申请一片内存空间，并返回T的指针，内存初始值为零；
	//entity2 :=new(TransferDetailEntity)
	//fmt.Println(entity2)
	//fmt.Println(&entity2)

	sourceStr := "{\"mchid\":\"323243234\",\"update_time\":\"2015-05-20T13:29:35.120+08:00\"}"
	// 1.Unmarshal的第一个参数是json字符串，第二个参数是接受json解析的数据结构。
	//第二个参数必须是指针，否则无法接收解析的数据，如stu仍为空对象StuRead{}
	//2.可以直接stu:=new(StuRead),此时的stu自身就是指针
	_ = json.Unmarshal([]byte(sourceStr), &entity)
	fmt.Println(entity)
	fmt.Println(&entity)
	fmt.Println(*entity.Mchid)
	fmt.Println(&entity.Mchid)

	//fmt.Println(*entity.UpdateTime)
	//fmt.Println(entity.UpdateTime.Format("2006-01-02 15:04:05"))
}

func test2() {
	var str string
	err2 := json.Unmarshal([]byte(nil), str)
	if nil != err2 {
		println("aaaaaaaaaaaaaa", err2)
	}
	println("aaaaaaaaaaaaaa", 111111111)

	stu := Stu{
		Name: "zhangsan", Age: 18, High: 178, sex: "男", Nb: true,
	}

	cla := new(Class)
	cla.Name = "5班"
	cla.Grade = 100
	stu.Class = cla

	stu.Class2 = *cla

	jsonStu, error := json.Marshal(stu)
	if error != nil {
		fmt.Println("生成json失败")
	}
	//jsonStu是[]byte类型，转化成string类型便于查看
	fmt.Println(string(jsonStu))

	stuInterface := StuInterface{
		Name: "zhangsan", Age: 18, High: 178, sex: "男", Nb: false,
	}
	jsonStuInterface, err := json.Marshal(stuInterface)
	if err != nil {
		fmt.Println("生成json失败")
	}
	//jsonStu是[]byte类型，转化成string类型便于查看
	fmt.Println(string(jsonStuInterface))

	s := Stu{}
	error = json.Unmarshal(jsonStu, &s)
	if error != nil {
		fmt.Println(error)
	}
	fmt.Println(s)

	//var i *int
	//*i = 10
	//fmt.Println(*i)
	//var pp int
	//panic(pp)

	var a, b, c int
	var d, e, f = 1, true, "aa"

	fmt.Printf("a=%d,b=%d,c=%d\n", a, b, c)
	fmt.Printf("d=%d,e=%t,f=%s\n", d, e, f)

	fmt.Printf("data is =%s", "Go语言")
	fmt.Printf("%s\n", []byte("Go语言"))

	// 结构体的初始化
	var v Stu
	i := &v
	i.Age = 19

	j := new(Stu)

	fmt.Printf("address i=%p,j=%p\n", &i, &j)

	p1 := new(int) //默认初始值为0
	*p1 = 98
	fmt.Println(p1)  //0xc00012e370
	fmt.Println(*p1) //98

	slice := make([]int, 0, 100)
	slice = append(slice, 1, 2)
	fmt.Println(slice)
}

func init() {
	fmt.Println("--------second--------")
}

type Stu struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
	High int    `json:"high"`
	Nb   bool   `json:"nb"`
	sex  string //变量首字母是大写才可以转成json

	Class *Class `json:"class"` // 指针更快，且能节省内存空间
	//普通struct类型
	Class2 Class `json:"class2"`
}
type StuInterface struct {
	Name interface{} `json:"name"`
	Age  interface{} `json:"age"`
	High interface{} `json:"high"`
	Nb   interface{} `json:"nb"`
	sex  interface{} //变量首字母是大写才可以转成json

	Class interface{} `json:"class"` // 指针更快，且能节省内存空间
}

type TransferDetailEntity struct {
	// 微信支付分配的商户号
	Mchid *string `json:"mchid"`
	// 商户系统内部的商家批次单号，在商户系统内部唯一
	OutBatchNo *string `json:"out_batch_no"`
	// 微信批次单号，微信商家转账系统返回的唯一标识
	BatchId *string `json:"batch_id"`
	// 申请商户号的appid或商户号绑定的appid（企业号corpid即为此appid）
	Appid *string `json:"appid"`
	// 商户系统内部区分转账批次单下不同转账明细单的唯一标识
	OutDetailNo *string `json:"out_detail_no"`
	// 微信支付系统内部区分转账批次单下不同转账明细单的唯一标识
	DetailId *string `json:"detail_id"`
	// PROCESSING:转账中。正在处理中，转账结果尚未明确   SUCCESS:转账成功   FAIL:转账失败。需要确认失败原因后，再决定是否重新发起对该笔明细单的转账（并非整个转账批次单）
	DetailStatus *string `json:"detail_status"`
	// 转账金额单位为“分”
	TransferAmount *string `json:"transfer_amount"`
	// 单条转账备注（微信用户会收到该备注），UTF8编码，最多允许32个字符
	TransferRemark *string `json:"transfer_remark"`
	// 商户appid下，某用户的openid
	Openid *string `json:"openid"`
	// 收款方姓名。采用标准RSA算法，公钥由微信侧提供 商户转账时传入了收款用户姓名、查询时会返回收款用户姓名
	UserName *string `json:"user_name,omitempty" encryption:"EM_APIV3"`
	// 转账发起的时间，按照使用rfc3339所定义的格式，格式为YYYY-MM-DDThh:mm:ss+TIMEZONE
	InitiateTime *time.Time `json:"initiate_time"`
	// 明细最后一次状态变更的时间，按照使用rfc3339所定义的格式，格式为YYYY-MM-DDThh:mm:ss+TIMEZONE
	UpdateTime *time.Time `json:"update_time"`
	// 如果转账失败则有失败原因
	FailReason *string `json:"fail_reason,omitempty"`
}
