package main

import (
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"net/url"
	"sort"
	"strings"
	"testing"
)

func TestEncode(t *testing.T) {
	// golang对“*”进行urlEnocde为”%2A“
	values := url.Values{}
	values.Add("key1", "&$-_.+!*'(),")
	values.Add("key2", "*")

	fmt.Println(values.Encode())
	fmt.Println(url.QueryEscape("key1=&$-_.+!*'(),&key2=*"))

	//m := map[string]interface{}
	//
	//GenSignature("hAVDkq8rKxyQdpJ5N7GAEF1xZOjxXzcX", )
}

func GenSignature(key string, data map[string]string) string {
	var (
		dataKey []string
		dataStr string
	)
	//排序
	for k, v := range data {
		if v == "" { //val为空字符串不参与签名
			continue
		}
		dataKey = append(dataKey, k)
	}
	sort.Strings(dataKey)
	//生成字典序k=v字符串
	for _, v := range dataKey {
		dataStr += v + "=" + data[v] + "&"
	}
	//拼接API秘钥,生成待签名字符串
	dataStr += "key=" + key
	//生成签名
	hashMD5 := md5.New()
	hashMD5.Write([]byte(dataStr))
	//转换成大写string
	return strings.ToUpper(hex.EncodeToString(hashMD5.Sum(nil)))
}
