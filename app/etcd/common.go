package main

import "sync"

var BusinessSymbolMap = map[string]struct{}{}
var Businesses = businesses{Value: map[string]*Business{}}

type businesses struct {
	Value  map[string]*Business
	rWLock sync.RWMutex
}

//已加入到etcd
//业务线配置
type Business struct {
	Name string   `json:"name"`
	Tag  []string `json:"tag"`
	//兜底帐号
	FinalAccount struct {
		Appid string `json:"appid"`
	} `json:"final_account"`
	rWLock sync.RWMutex
}