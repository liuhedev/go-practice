package util

import (
	"fmt"
	"net/url"
	"testing"
)

const priKey = "-----BEGIN RSA PRIVATE KEY-----MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQC/3vvhuvT3WajIT9wnMeqYQa5YF43xTTYdmZhIcct7Fd4t5ztxoNmLohG0oKmTfuR7SBnHw7ueRBQqQzd8ey5biRuuj5CxAyqWdtKeQoOEZ50JMF7TDsNLb5EZlkCFtJxOilHivlB1RImrkC6dkOPmh8oH/d3ETcVjgKgQmxL41BSTM0wSe0HBcrN+sZwq0rdHQ9D9Sds/L4IKN/4xIgAyocbexG0C9pLwYXx/wfzuo8vZD08tzDLFxLBxaYkot7L5ZJkGPZz8Y91xABcScj1+BJBhJnbfQfaO4njHWaL74rbQrEQci2D+u3uqaJeQfUSLKuLOam1fthNeLcDAU1svAgMBAAECggEAII5FTtfaGN4ttU9Xm47sr0j1vBzDCN6BACS/ogss64WaXU+O1ojkYLzvJCbenxg4zHI/bZXYj9IdWc5t68E67G0nNUdG1cIjx8QBmLjQd/bcvmJmtIkcugAELsM/z3AF1Y3CtyZzoHIwqdudDT84bDU+In0A0/4eL/P0kFy/q1iFjXwqFHNh5Ca22Vee13FCKcFS2dOivgchD0jL75WTmoFVyoOTl0vB4VLi2/C4nrOdM29P+sCcPuSacL35sMmwarNEPNHOExJom8GuxPTHfOO3QLKnu9DzAhUtq7b+PyhWg34Pu4y0YfIX2LsDu+qXQa0sh4l9pJFEAFUM0+jvAQKBgQDrJuW5ZPQ6aESrk+8/vklDLwfNq758JspwwPMfLhArINdzPam0XNoA/zeoTlE2Cj4yWzeCHbWX3KiP/rFtyA5M+EyKc4PfUYmOEy7/f74KRfwyEKkWPBeLmGFyt/haIlnYDJA2JwET70TLqgCM3BU+DBsci5OpGYGHqkmgroh4hwKBgQDQ4cQBiR6kFwxCHUH9C0qdGIXCmfWIrhWGpAX+fyrOb/kZhrx38Cn81FKecRT0NkGYl7UZFCBjq5OfxcV+2/rGaOHvQwYbHWWk3WVK/RLyWgfnmwknlDtjhrT3TkJ7ijBWNLfKVKfFLAukVW2l/KP+HD46LnD78GfqsvIwEgc6GQKBgQDdIiEaLSO24WLibyWoLgu7UDjoYr+pBNmPGwQGqEywIhw51R0H7eFc2qS+c5fPV+aReFJE3Cdu/iTN9Ndpw54fJ8ji0iqLEaRnpQanYruV8aZudlhr/mqt7ciGFxdHu9H9aCMdqaRL2NA7Lk2CKV0Ykk3UlDVx6fhzWeIPZtITdQKBgBbx5dUBWfNOcNNGYXxJZD3TdsYEoXsNHGI43EuzbmFU3XkXWtaV9BbIv/aVl2aoGxaG+6awlko8i6/OSr5/rZFj7gWZSC9sUEEG7NyK5iznBN1FI4FMgCWTo4dS+myVpm+hxNqd5/v5taGsnKnSEbWVJqS/ezvBkbivRXlQUajpAoGAZu3oxKLmEVXoEQ1veD8cYi/M965zoDf+nHO2XVpGFOr6bRgryuufEXxoC58pqYkP+NNRuPwLCe/QtIjFVNOobqupTrHa/bJ/RsgoTlSS/ozsXfOG0hMMMiMfYR+0F6yY/xsnhvwm70CqUKOdyc4+lXIhVosEyMR6VDIwe45sT40=-----END RSA PRIVATE KEY-----"
const pubKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAv9774br091moyE/cJzHqmEGuWBeN8U02HZmYSHHLexXeLec7caDZi6IRtKCpk37ke0gZx8O7nkQUKkM3fHsuW4kbro+QsQMqlnbSnkKDhGedCTBe0w7DS2+RGZZAhbScTopR4r5QdUSJq5AunZDj5ofKB/3dxE3FY4CoEJsS+NQUkzNMEntBwXKzfrGcKtK3R0PQ/UnbPy+CCjf+MSIAMqHG3sRtAvaS8GF8f8H87qPL2Q9PLcwyxcSwcWmJKLey+WSZBj2c/GPdcQAXEnI9fgSQYSZ230H2juJ4x1mi++K20KxEHItg/rt7qmiXkH1EiyrizmptX7YTXi3AwFNbLwIDAQAB"

func TestSign(t *testing.T) {
	data := url.Values{}
	data.Set("buyerId", "30289183")
	data.Set("outTradeNo", "qa202201210001898373494310_t")
	data.Set("payAmount", "100")
	data.Set("tradeType", "JSAPI")
	data.Set("appId", "wxb2416dec2ef69bf4")
	data.Set("appId", "wxb2416dec2ef69bf4")
	data.Set("productId", "qa202201210001898373494310_t")
	data.Set("openId", "oSvLHwFIlV_Jnj-zzbtFV3Ctk_Kc")
	data.Set("body", "")
	data.Set("sceneInfo", "{\\\"h5_info\\\":{}}")
	data.Set("clientIp", "120.235.142.170")
	data.Set("sourceId", "QSB")
	data.Del("sign")
	for k, _ := range data {
		v := data.Get(k)
		if len(v) <= 0 {
			data.Del(k)
		}
	}
	str, _ := url.QueryUnescape(data.Encode())
	fmt.Println("加密前：", str)

	rsa, _ := XSign(priKey, str)
	fmt.Println("加密后：", rsa)

	//err := rsa.Verify(str, sign)
	//fmt.Print(err)

	s := []string{"aa", "bb"}
	for i, v := range s {
		fmt.Println(i, v)
	}
}
