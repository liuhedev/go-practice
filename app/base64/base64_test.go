package base64

import (
	"encoding/base64"
	"fmt"
	"net/url"
	"testing"
)

func TestName(t *testing.T) {
	sign := "WECHATPAY/SIGNTEST/lsBFtfEHqEYicuCIRzx3v2nU/3oAfrHDe7XNQ4cMBleRnfZTUExhp6jqqAZyt7t/kncC/j0UP9Gh1uDdsW5CwOtGELJ4SKO83CwTef97F2NaeGFxTRvJT1XuOsmR4/nDaIHmYDrHuWhpG3O2VQ3EuxBmldowAAFG7LC0K36Qfp+YuXUUC1MzV3qP8Uci5lHPisS40cF5RpZRopi3tJGbZxHp9A6NpogrN3I2bv3szW2W9XIChy8f8Y4ymo8UqIXU2LbnaGUyZnNu0qtX6U+Z+hrxYMXhfu3s9G5/JkPv2U1iPHbvERRXXKKsy3Xc0B94zvC/Mo3DYQ2Skw"
	decodeString, err := base64.StdEncoding.DecodeString(sign)
	fmt.Println(decodeString)
	fmt.Println(err)
	// illegal base64 data at input byte 353
}

func TestUrlEncode(t *testing.T) {
	data := url.Values{}
	data.Set("userid", "11100")
	data.Set("amount", "100")
	data.Set("appid", "232424234")
	data.Set("name", "张三")
	// golang sdk net/url包下的Encode方法，根据key进行排序，并会把key和value单独Enocde，中间"="不encode
	encode := data.Encode()
	fmt.Println("encode:", encode)

	// golang sdk net/url包下的QueryEscape方法，针对入参进行urlencode编码
	escapeStr := url.QueryEscape(encode)
	fmt.Println("escapeStr:", escapeStr)

	// golang sdk net/url包下的QueryUnescape方法，针对入参进行urlencode解码
	unescapeStr, _ := url.QueryUnescape(encode)
	fmt.Println("unescapeStr:", unescapeStr)
}
