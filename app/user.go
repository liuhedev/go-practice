package main

type User struct {
	name   string `json:"username"`
	age    int    `json:"age"`
	gender bool   `json:"gender"`
}
