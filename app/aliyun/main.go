// This file is auto-generated, don't edit it. Thanks.
package main

import (
	"os"
	"time"

	bssopenapi20171214 "github.com/alibabacloud-go/bssopenapi-20171214/v3/client"
	openapi "github.com/alibabacloud-go/darabonba-openapi/v2/client"
	util "github.com/alibabacloud-go/tea-utils/v2/service"
	"github.com/alibabacloud-go/tea/tea"
)

type Bill struct {
	StatusCode int  `json:"statusCode"`
	Body       Body `json:"body"`
}

type Body struct {
	Code      string `json:"Code"`
	Data      Data   `json:"Data"`
	RequestID string `json:"RequestId"`
	Success   bool   `json:"Success"`
}

type Data struct {
	Items Items `json:"Items"`
}

type Items struct {
	Item []Item `json:"Item"`
}

type Item struct {
	CostUnit     string  `json:"CostUnit"`
	PretaxAmount float64 `json:"PretaxAmount"`
}

func CreateClient(accessKeyId *string, accessKeySecret *string) (_result *bssopenapi20171214.Client, _err error) {
	config := &openapi.Config{
		// 必填，您的 AccessKey ID
		AccessKeyId: accessKeyId,
		// 必填，您的 AccessKey Secret
		AccessKeySecret: accessKeySecret,
	}
	// 访问的域名
	config.Endpoint = tea.String("business.aliyuncs.com")
	_result = &bssopenapi20171214.Client{}
	_result, _err = bssopenapi20171214.NewClient(config)
	return _result, _err
}

func _main(args []*string) (_err error) {
	client, _err := CreateClient(tea.String("akid"), tea.String("aksec"))
	if _err != nil {
		return _err
	}

	YesMonth := time.Now().AddDate(0, -1, 0)
	YesMonthFmt := YesMonth.Format("2006-01")

	queryAccountBillRequest := &bssopenapi20171214.QueryAccountBillRequest{
		BillingCycle: tea.String(YesMonthFmt),
	}
	runtime := &util.RuntimeOptions{}
	tryErr := func() (_e error) {
		defer func() {
			if r := tea.Recover(recover()); r != nil {
				_e = r
			}
		}()
		//就是这！！
		resp, _err := client.QueryAccountBillWithOptions(queryAccountBillRequest, runtime)
		if _err != nil {
			return _err
		}
		//我就是想把resp的值  取出来CostUnit PretaxAmount
		println(resp.Body.Data.Items.Item[0].CostUnit)
		println(resp.Body.Data.Items.Item[0].PretaxAmount)

		return nil
	}()

	if tryErr != nil {
		var error = &tea.SDKError{}
		if _t, ok := tryErr.(*tea.SDKError); ok {
			error = _t
		} else {
			error.Message = tea.String(tryErr.Error())
		}
		_, _err = util.AssertAsString(error.Message)
		if _err != nil {
			return _err
		}
	}
	return _err
}

func main() {
	//go run main.go param1 param2
	slice := tea.StringSlice(os.Args[1:])
	for i, s := range slice {
		println("input", i, *s)
	}

	err := _main(slice)
	if err != nil {
		panic(err)
	}
}
