package main

import (
	"errors"
	"fmt"
)

func init() {
	println("hello baby")
}
func main() {

	//busTypes := strings.Split("wa", ",")
	//for i := range busTypes {
	//	fmt.Println("aa", busTypes[i])
	//}
	//funcMap()
	funcMap2()
	//funcSlice()

	const constData = "hello"

	//fmt.Println(user)
	//for  {
	//	go func() {
	//		funcName()
	//		time.Sleep(5 * time.Second)
	//	}()
	//}
	//	data := []byte(`{
	//  "person": {
	//    "name":{
	//      "first": 1,
	//      "last": "Bugaev",
	//      "fullName": "Leonid Bugaev"
	//    },
	//    "github": {
	//      "handle": "buger",
	//      "followers": 109
	//    },
	//    "avatars": [
	//      { "url": "https://avatars1.githubusercontent.com/u/14009?v=3&s=460", "type": "thumbnail" }
	//    ]
	//  },
	//  "company": {
	//    "name": "Acme"
	//  }
	//}`) //注：为节省篇幅，data结构参考go-simplejson
	//
	//	//get total
	//
	//	total, _ := jsonparser.GetInt(data, "person", "name", "first")
	//	fmt.Println(total)
	////funcName()
	//i := c()
	//print(i)
	// 延迟处理的函数
	//defer func() {
	//	// 发生宕机时，获取panic传递的上下文并打印
	//	if ok := recover(); ok != nil {
	//		fmt.Println("recover")
	//	}
	//}()
	//
	//panic("crash") //手动触发宕机
	//
	//b1 := false
	//
	//println("b1 addr:", &b1)
	//println("b1 value:", b1)

	//user := User{"liuhe", 28, true}
	//
	//user2 := User{name: "wagnwu", age: 26}
	//fmt.Println(user)
	//fmt.Println(user2)
	//fmt.Println(user.name)
	//
	//pointStruct := PointStruct()
	//println(pointStruct)
	//fmt.Println(pointStruct)
	//
	////切片
	//s := []int{1, 2, 3}
	//println(s)
	//fmt.Println(s)
	//
	//ints := make([]int, 10, 10)
	//fmt.Println(&ints)
	//ints[3] = 3
	//ints = append(ints, 2)
	//fmt.Println(ints)
	//fmt.Println(&ints)
	//ints = append(ints, 1, 2)
	//fmt.Println("%p",&ints)

	return

}

func funcSlice() {
	//2标识，切片容量为2时候，进行扩容
	str := make([]string, 0, 2)
	str = append(str, "a")
	str = append(str, "a")
	str = append(str, "a")
	str = append(str, "a")
	str = append(str, "a")
	str = append(str, "a")
	fmt.Println(str)
}

func funcMap() {

	user := make(map[string]interface{})
	user["name"] = "liuhe"
	user["age"] = 18

	user2 := map[string]string{}

	fmt.Println(user2)
}

func funcMap2() {

	var user map[string]interface{}
	//user = map[string]interface{}{} // 不声明，直接使用会报错npe
	user["name"] = "liuhe"
	user["age"] = 18

	user2 := map[string]string{}

	fmt.Println(user2)
}

func WxDeductionReq(amount int64, spbillCreateIp string, contractId string, body string, tradeNoTmp string) error {
	var task = func() error {
		fmt.Println("执行到此处")
		return tempError()
	}
	err := RetryTimes(2, task)
	return err
}

// 重试，限制次数
func RetryTimes(tryTimes int, callback func() error) (err error) {
	fmt.Println("==进入重试进制==")
	for i := 1; i <= tryTimes; i++ {
		err = callback()
		if err == nil {
			return nil
		}
		fmt.Println("wxDeductionReq", "retryCount", tryTimes, "curCount", i, "err", err)
	}
	return err
}

func tempError() error {
	fmt.Println("调用到我了~~~")
	return errors.New("Post \"https://api.mch.weixin.qq.com/pay/pappayapply\": http: server closed idle connection")
	//return nil
}

func funcName() int {
	defer func() {
		println("defer1")
	}()
	/* 创建切片 */
	numbers := []int{0, 1, 2, 3, 4, 5, 6, 7, 8}
	printSlice(numbers)
	defer func() {
		println("defer2")
	}()
	return len(numbers)

}

func printSlice(x []int) {
	fmt.Printf("len=%d cap=%d slice=%v\n", len(x), cap(x), x)
}

func calc(index string, a, b int) int {
	ret := a + b
	fmt.Println(index, a, b, ret)
	return ret
}

func c() (i int) {
	defer func() { i++ }()
	return 1
}

//func PointStruct() *User {
//
//	var structPointer *User
//	user := User{"liuhe", 28, true}
//	structPointer = &user
//
//	return structPointer
//
//}
