package nil

import (
	"fmt"
	"testing"
)
func TestNil(t *testing.T) {
	//./nil_test.go:9:17: invalid operation: nil == nil (operator == not defined on nil)
	//fmt.Println(nil == nil)

	var arr []int
	var num *int
	fmt.Printf("%p\n", arr)
	fmt.Printf("%p", num)

	aa:="23234"
	fmt.Println(len(aa))
}
