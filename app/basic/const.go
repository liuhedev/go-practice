package main

import (
	fmt "fmt"
)

//const (
//	_ = iota
//	Normal
//	Deleted
//)

func main() {

	go func() {
		fmt.Println()
	}()
	a := 2
	fmt.Println(a)
	//test01()
	//test02()
	//test03()
}

//
//func test03() {
//	aa := Aa{
//		100,
//	}
//	aa.mod(2)
//}
//
//func test02() {
//	fmt.Println(Normal)
//
//}
//func test01() {
//	/* nextNumber 为一个函数，函数 i 为 0 */
//	nextNumber := getSequence()
//
//	/* 调用 nextNumber 函数，i 变量自增 1 并返回 */
//	fmt.Println(nextNumber())
//	fmt.Println(nextNumber())
//	fmt.Println(nextNumber())
//
//	/* 创建新的函数 nextNumber1，并查看结果 */
//	nextNumber1 := getSequence()
//	fmt.Println(nextNumber1())
//	fmt.Println(nextNumber1())
//}
//
//type Aa struct {
//	a int
//}
//
//func (a Aa) mod(b int) {
//	fmt.Println(a.a)
//	fmt.Println(a.a - b)
//}
//
//func getSequence() func() int {
//	i := 0
//	return func() int {
//		i += 1
//		return i
//	}
//}

//
//func main() {
//
//	const (
//		a = iota //0
//		b        //1
//		c        //2
//		d = "ha" //独立值，iota += 1
//		e        //"ha"   iota += 1
//		f = 100  //iota +=1
//		g        //100  iota +=1
//		h = iota //7,恢复计数
//		i        //8
//	)
//	fmt.Println(a, b, c, d, e, f, g, h, i)
//
//	var aa int = 10
//	var bb = aa
//
//	println(&aa, &bb)
//
//	if aa == bb {
//		fmt.Println("i'm sure")
//	} else {
//		fmt.Println("not euqual")
//	}
//
//	var grade string = "B"
//	var marks int = 90
//	switch marks {
//	case 90:
//		grade = "A"
//	case 80:
//		grade = "B"
//	case 50, 60, 70:
//		grade = "C"
//	default:
//		grade = "D"
//	}
//	fmt.Printf("grade=%s\n\n", grade)
//
//	multData := []string{"1", "2", "3", "4"}
//	var stringData[]string
//	for _, value := range multData {
//		stringData = append(stringData, fmt.Sprintf("(%s)", value))
//	}
//
//	data := strings.Join(stringData, ",")
//
//	sql := "insert into pay_order (bus_type) values " + data
//
//	fmt.Println("data=", sql)
//}
