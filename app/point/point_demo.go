package main

import "fmt"

type Point struct {
	b int
}

func main() {
	var a, b int
	fmt.Println("a的地址：", &a, " a的值：", a)
	fmt.Println("b的地址：", &b, " b的值：", b)

	test01(a)

	//test02(b)
}

func test02(b int) {
	point2 := Point{}
	point2.swapData3(&b)
	fmt.Println("333---b的地址：", &b, " a的值：", b, "p.b的地址：", &point2.b, "p.b的值：", point2.b)
	point2.swapData4(&b)
	fmt.Println("444---b的地址：", &b, " a的值：", b, "p.b的地址：", &point2.b, "p.b的值：", point2.b)
}

func test01(a int) {
	point1 := Point{}
	point1.swapData(a)
	fmt.Println("111---a的地址：", &a, " a的值：", a, "p.b的地址：", &point1.b, "p.b的值：", point1.b)
	point1.swapData2(a)
	fmt.Println("222---a的地址：", &a, " a的值：", a, "p.b的地址：", &point1.b, "p.b的值：", point1.b)
}

func (p Point) swapData(a int) {
	a = 100
	p.b = 200
}

func (p *Point) swapData2(a int) {
	a = 100
	p.b = 200
}

func (p Point) swapData3(b *int) {
	*b = 100
	p.b = 200
}

func (p *Point) swapData4(b *int) {
	*b = 100
	p.b = 200
}

//a的地址： 0xc000020098  a的值： 0
//b的地址： 0xc0000200a0  b的值： 0
//111---a的地址： 0xc000020098  a的值： 0 p.b的地址： 0xc0000200a8 p.b的值： 0
//222---a的地址： 0xc000020098  a的值： 0 p.b的地址： 0xc0000200a8 p.b的值： 200
//333---b的地址： 0xc0000200a0  a的值： 100 p.b的地址： 0xc0000200c0 p.b的值： 0
//444---b的地址： 0xc0000200a0  a的值： 100 p.b的地址： 0xc0000200c0 p.b的值： 200