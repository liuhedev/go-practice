package main

import (
	"fmt"
	"runtime/debug"
	"strconv"
	"time"
)

func main() {

	fmt.Println(strconv.FormatInt(time.Now().Unix(), 16))

	//
	//println("111 -> coming")
	//go func() {
	//	// mock panic
	//	defer mockErr()
	//	panic("222 -> gameOver")
	//}()
	//time.Sleep(1 * time.Second)
	//println("111 -> go on")
	//defer mockErr()
	//err := doSomeThings()
	//if nil != err {
	//	return
	//}
	//defer func() {
	//	println("333 -> defer")
	//}()
	//println("111 -> end")
}

func doSomeThings() error {
	return fmt.Errorf("mockerr")

}

func mockErr() {
	if err := recover(); err != nil {
		// 输出panic信息，eg："runtime error: index out of range [24] with length 24"
		fmt.Println(err)
		// 输出详细的堆栈信息
		fmt.Println(string(debug.Stack()))
	}
}
