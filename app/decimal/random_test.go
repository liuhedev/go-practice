package decimal

import (
	"math/rand"
	"testing"
	"time"
)

func TestRandom(t *testing.T) {
	println(RandInt())
}

func RandInt() int {

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	//Intn 从默认 Source 返回半开区间 [0,n) 中的非负伪随机数，作为 int。如果 n <= 0，它会Panics。
	return r.Intn(100)
}

