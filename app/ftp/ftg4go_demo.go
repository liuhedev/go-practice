package main

import (
	"fmt"
	ftp4go "github.com/shenshouer/ftp4go"
	"os"
)

var (
	DOWNLOAD_FILENAME_LIST = []string{"RECPDF_15011289060022_20240414.zip"}
)

func main() {
	ftpClient := ftp4go.NewFTP(0) // 1 for debugging
	//connect
	_, err := ftpClient.Connect("172.16.80.64", 22, "")
	if err != nil {
		fmt.Println("The connection failed")
		os.Exit(1)
	}
	defer ftpClient.Quit()

	_, err = ftpClient.Login("pingan", "Lavie261602!23", "")
	if err != nil {
		fmt.Println("The login failed")
		os.Exit(1)
	}

	//Print the current working directory
	var cwd string
	cwd, err = ftpClient.Pwd()
	if err != nil {
		fmt.Println("The Pwd command failed")
		os.Exit(1)
	}
	fmt.Println("The current folder is", cwd)

	for _, filename := range DOWNLOAD_FILENAME_LIST {
		fmt.Println("download_start", filename)
		// get the remote file size
		remoteFilePath := cwd + "/" + filename
		localFilePath := "./ftptest/" + remoteFilePath
		size, err := ftpClient.Size(remoteFilePath)
		if err != nil {
			fmt.Println("download_getSizeFailed", err.Error())
			return
		}
		fmt.Println(filename+" size ", size)
		// start resume file download
		if err = ftpClient.DownloadResumeFile(remoteFilePath, localFilePath, false); err != nil {
			fmt.Println("download_DownloadResumeFileFailed", err.Error())
		}
		fmt.Println("download_end", filename)
	}
}
