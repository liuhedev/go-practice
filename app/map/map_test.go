package main

import (
	"fmt"
	"testing"
)

func TestMapDemo(t *testing.T) {
	m := make(map[string]string)
	m["aa"] = "2323"
	//当map的key不存在的时候，它的返回值为这个类型的默认返回值，如空串
	println(fmt.Sprintf("cc value==%s==", m["cc"]))

	value, ok := m["aa"]
	if !ok {
		println("not exist")
		return
	}
	println("exist", value)
}

func TestMapValue(t *testing.T) {
	var m3 = map[string]string{
		"SH": "商户子账户",
		"00": "会员子账户",
	}

	s := m3["SH"]
	println(s)

	v, ok := m3["00"]
	fmt.Println(v, ok)

}
