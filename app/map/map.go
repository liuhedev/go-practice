package main

import "fmt"

func main() {
	//m := make(map[string]OpenId, 0)
	//m["wx1e5232f351f4fa7a"] = OpenId{"ocnR_xLH3KUMMGKMQ4WUr6HrlL-I", 2, 1}
	//m["wx0b3d82f245095547"] = OpenId{"ocnR_xLH3KUMMGKMQ4WUr6HrlL-I", 2, 1}
	//m["wx30f0daaedccb450d"] = OpenId{"ocnR_xLH3KUMMGKMQ4WUr6HrlL-I", 2, 1}
	//m["wx7b56db4bc85e4cf1"] = OpenId{"ocnR_xLH3KUMMGKMQ4WUr6HrlL-I", 2, 1}
	//m["wx96525cfd0ec9056d"] = OpenId{"ocnR_xLH3KUMMGKMQ4WUr6HrlL-I", 2, 1}
	//
	//for key, value := range m {
	//	fmt.Println("key", key, "value", value)
	//}

	// map创建的几种方式
	// 方式一：声明直接make
	map1 := make(map[string]string)
	map1["map1"] = "map1"

	//方式二：声明此时为nil，读取不抛npe,赋值的时候会报npe；使用的时候需要make，比如map2=make(map[string]string)
	var map2 map[string]string

	//方式三：直接赋值
	var map3 map[string]string = map[string]string{
		"map3": "map3",
	}

	fmt.Println(map1["map1"])
	fmt.Println(map2["map2"])
	fmt.Println(map3["map3"])

	fmt.Println("---------")

	//// panic: assignment to entry in nil map
	//map2["mp2"] = "mp2"
	//fmt.Println(map2["map2"])

	map2 = make(map[string]string)
	map2["map2"] = "map2"
	fmt.Println(map2["map2"])
}

type OpenId struct {
	openId          string
	interactionIn48 int
	subStatus       int
}
