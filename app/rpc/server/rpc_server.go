package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	pt "liuhe.io/go_practice/app/rpc/my_grpc_proto"
	"net"
)

const (
	post = "127.0.0.1:18881"
)

//对象要和proto内定义的服务一样
type server struct{}

//实现RPC SayHello 接口
func (this *server) SayHello(ctx context.Context, in *pt.HelloRequest) (*pt.HelloReplay, error) {
	return &pt.HelloReplay{Message: "hello " + in.Name}, nil
}

//实现RPC GetHelloMsg 接口
func (this *server) GetHelloMsg(ctx context.Context, in *pt.HelloRequest) (*pt.HelloReplay, error) {
	return &pt.HelloReplay{Message: "this is from server ok~~~~!"}, nil
}

func main() {
	// 1、监听网络
	ln, err := net.Listen("tcp", post)
	if err != nil {
		fmt.Println("网络异常", err)
	}

	// 2、创建一个grpc的句柄
	srv := grpc.NewServer()

	// 3、将server结构体注册到 grpc服务中
	pt.RegisterHelloServerServer(srv, &server{})

	// 解决grpcui 反射报错问题
	reflection.Register(srv)

	// 4、监听grpc服务
	err = srv.Serve(ln)
	if err != nil {
		fmt.Println("网络启动异常", err)
	}
}
