package main

/* 定义结构体 */
type Circle struct {
	radius float64
}

type Rect struct {
	length float64
}

func main() {
	//var c1 Circle
	//c1.radius = 10.00
	//fmt.Println("圆的面积 = ", c1.getArea())
	//
	//var rect Rect
	//rect.length = 15.00
	//fmt.Println("矩形的面积 = ", rect.getArea())

	arr := []int{1, 23, 4}
	for _,v := range arr {
		println(v)
	}
	//fmt.Println(arr)
	//
	//arr = append(arr, 100)
	//fmt.Println(arr)
	//
	//arr2 := [3]int{4, 5, 6}
	//fmt.Println(arr2)

}

//该 method 属于 Circle 类型对象中的方法
func (c Circle) getArea() float64 {
	//c.radius 即为 Circle 类型对象中的属性
	return 3.14 * c.radius * c.radius
}

//该 method 属于 Rect 类型对象中的方法
func (c Rect) getArea() float64 {
	//c.radius 即为 Circle 类型对象中的属性
	return c.length * c.length
}
