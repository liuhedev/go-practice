package main

import (
	"fmt"
	"io/ioutil"
	"net/url"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	pathTest2()

}

func read3(path string) string {
	fi, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	defer fi.Close()
	fd, err := ioutil.ReadAll(fi)
	return string(fd)
}

func pathTest1() {
	//fileName := "/Users/liuhe/go/src/gitee.com/liuhedev/go-practice/app/file/1.txt"
	//s := read3(fileName)
	//fmt.Println(s)

	files := "/Users/liuhe/Downloads/802120059600001_20220529_20220529140522177.txt"
	filenameWithSuffix := filepath.Base(files)
	fmt.Println("filenameWithSuffix = ", filenameWithSuffix) // 返回路径的最后一个元素，即文件名

	fileSuffix := filepath.Ext(files) // 返回文件的扩展名，带.，如".zip"
	fmt.Println("fileSuffix = ", fileSuffix)

	fmt.Println(fileSuffix == ".zip")

	var filenameOnly = strings.TrimSuffix(filenameWithSuffix, fileSuffix)
	fmt.Println("filenameOnly =", filenameOnly)

	//name := "323_sdsdf.txt"
	//split := strings.Split(name, "_")
	//fmt.Println(split[1])
	//fmt.Println(split[2]) // index out of range

	trimFileName := strings.Replace(filenameWithSuffix, ".zip", "", -1)
	trimFileName = strings.Replace(trimFileName, ".txt", "", -1)
	split := strings.Split(trimFileName, "_")
	fileTimestamp := split[len(split)-1]
	if len(fileTimestamp) == 17 {
		fmt.Println("直联文件")
	} else {
		fmt.Println("非直联文件，不处理")
	}

	detail := "20220524|802120059600001|0001|99|1001|T999|156|20220525|355937|8|621700*********9666|355937|202205240008846692396386yl|0524183935|6692396386yl|000000|0|0|01|202205240008846692396386yl||||{\"BankNo\":\"0105\",\"TradeSubType\":\"0001\",\"OtherFeeAmt\":0,\"AllTradeTwowayFeeAmt\":8}|"
	detailSplit := strings.Split(detail, "|")
	fmt.Println(len(detailSplit))
}

func pathTest2() {
	//代付账单
	url1 := "http://sfj.chinapay.com/dac/FileDownServlet?filename=596082303035001_20230330_20230406164400919700.txt.zip"
	//交易账单
	url2 := "http://payment.chinapay.com/DOWFL/mer/downloadFile?filePath=2AA6B44D8CD2B675EE175C83EDE99B372CD59C20FB8FBAE1E0C1BFA7F5849682E6023AF87F1012E6B2EFEE29AAB2014F3AFE2FF77C90EE0424E5067762082B46/802120059600001_20230401_20230401140520189.txt"
	println(filepath.Base(url1))
	println(filepath.Base(url2))

	parseUrl1, _ := url.Parse(url1)
	println(parseUrl1.RawQuery)
	parseUrl2, _ := url.Parse(url2)
	println(parseUrl2.RawQuery)

	println(filepath.Base(strings.Split(parseUrl1.RawQuery, "=")[1]))
	println(filepath.Base(strings.Split(parseUrl2.RawQuery, "=")[1]))


}
